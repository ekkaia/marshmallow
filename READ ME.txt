  __  __          _____   _____ _    _ __  __          _      _      ______          __
 |  \/  |   /\   |  __ \ / ____| |  | |  \/  |   /\   | |    | |    / __ \ \        / /
 | \  / |  /  \  | |__) | (___ | |__| | \  / |  /  \  | |    | |   | |  | \ \  /\  / / 
 | |\/| | / /\ \ |  _  / \___ \|  __  | |\/| | / /\ \ | |    | |   | |  | |\ \/  \/ /  
 | |  | |/ ____ \| | \ \ ____) | |  | | |  | |/ ____ \| |____| |___| |__| | \  /\  /   
 |_|  |_/_/    \_\_|  \_\_____/|_|  |_|_|  |_/_/    \_\______|______\____/   \/  \/  

POUR LANCER LE PROJET :

1) Extraire les fichiers de l'archive WinRAR.

2) Installer NodeJS

3) Ouvrir la console NodeJS dans le dossier du projet

4) Taper la commande suivante : npm install --save socket.io express body-parser mongoose pug signale
5) Taper ensuite la commande : npm i

6) Lancer le projet avec la commande : node server.js

7) Se rendre à l'adresse : localhost:3020
